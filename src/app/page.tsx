import { promises as fs } from "fs";
import { redirect } from "next/navigation";
import styles from "./page.module.css";

export async function createLobby(formData: FormData) {
  "use server";

  // If creating Lobby
  if (formData.get("create")) {
    // TODO: Rework this with a MongoDb Database, cause Serverless is annoying with files writing
    //console.log("Creating Lobby");
    const file = await fs.readFile(process.cwd() + "/tmp/lobbys.json", "utf8");
    const data = JSON.parse(file);
    //console.log(Object.keys(data.lobbys));

    // Generate lobby id
    let lobbyID = "";
    do {
      lobbyID = generateLobbyID(16);
    } while (Object.keys(data.lobbys).includes(lobbyID));

    data.lobbys[lobbyID] = {
      active: true,
      date: new Date().toLocaleString("fr-FR"),
    };

    await fs.writeFile(
      process.cwd() + "/tmp/lobbys.json",
      JSON.stringify(data, null, 2),
    );

    redirect(`/lobby/${lobbyID}`);
  } else {
    // If Joining Lobby
    console.log("Joining Lobby");
    if (!formData.get("lobbyID")) {
      //Error Message Code
      /*return {
        success: false,
        error: {
          errcode: "001",
          errMess: "Please give a Lobby ID",
        },
      };*/
    } else {
      // TODO: Check if lobby exists
      redirect(`/lobby/${formData.get("lobbyID")}`);
    }
  }
}

const generateLobbyID = (length = 16) => {
  // Declare all characters
  let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  // Pick characers randomly
  let str = "";
  for (let i = 0; i < length; i++) {
    str += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return str;
};

export default function Home() {
  return (
    <main className={styles.main}>
      <form className={styles["lobby-mode-selection"]} action={createLobby}>
        <div className={styles["lobby-mode-block"]}>
          <div className={styles["lobby-mode-block-content"]}>
            <button
              className={styles.btns}
              type={"submit"}
              name={"create"}
              value={1}
            >
              Create Lobby
            </button>
          </div>
        </div>
        <div
          id={styles["join-lobby-block"]}
          className={styles["lobby-mode-block"]}
        >
          <div className={styles["lobby-mode-block-content"]}>
            <input type="text" name={"lobbyID"} placeholder="Lobby ID" />
            <button
              className={styles.btns}
              type={"submit"}
              name={"join"}
              value={2}
            >
              Join Lobby
            </button>
          </div>
        </div>
      </form>
    </main>
  );
}
