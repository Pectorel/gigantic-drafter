import Link from "next/link";
import Image from "next/image";
import DrafterLogo from "@/assets/img/drafter-logo.png";
import styles from "./header.module.css";


export default function Header(){

  return <header id={styles["header"]}>
    <nav id={styles["nav"]}>
      <Link href={"/"}>
        <Image src={DrafterLogo} height={75} alt={"Gigantic Drafter Logo"}></Image>
      </Link>
    </nav>
  </header>

}