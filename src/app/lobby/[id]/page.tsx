export default function Page({ params }: { params: { id: string } }) {
  return (
    <section>
      <section>
        <div>Player 1</div>
        <div>Player 2</div>
        <div>Player 3</div>
        <div>Player 4</div>
        <div>Player 5</div>
      </section>

      <section>
        <div>Player 1</div>
        <div>Player 2</div>
        <div>Player 3</div>
        <div>Player 4</div>
        <div>Player 5</div>
      </section>
    </section>
  );
}
